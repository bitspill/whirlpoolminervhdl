-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity AddKey is
	port (
		input : in std_logic_vector(511 downto 0);
		key : in std_logic_vector(511 downto 0);
		output : out std_logic_vector(511 downto 0)
	);
end entity AddKey;

architecture RTL of AddKey is
	
begin
	output <= input xor key;
end architecture RTL;
