-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Adder32Bits is
	port(
		a : in  std_logic_vector(31 downto 0);
		b : in  std_logic_vector(31 downto 0);
		c : out std_logic_vector(31 downto 0)
	);
end entity Adder32Bits;

architecture RTL of Adder32Bits is
begin
	c <= std_logic_vector(unsigned(a) + unsigned(b));
end architecture RTL;
