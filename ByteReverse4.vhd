-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ByteReverse4 is
	port(
		input  : in  std_logic_vector(4 * 8 - 1 downto 0);
		output : out std_logic_vector(4 * 8 - 1 downto 0)
	);
end entity ByteReverse4;

architecture RTL of ByteReverse4 is
begin
	output <= input(1 * 8 - 1 downto 1 * 8 - 8) & input(2 * 8 - 1 downto 2 * 8 - 8) & input(3 * 8 - 1 downto 3 * 8 - 8) & input(4 * 8 - 1 downto 4 * 8 - 8);
end architecture RTL;
