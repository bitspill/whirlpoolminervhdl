-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity EBox is
	port(
		input  : in  std_logic_vector(3 downto 0);
		output : out std_logic_vector(3 downto 0)
	);
end entity EBox;

architecture RTL of EBox is
	signal t0_0 : std_logic;
	signal t0_1 : std_logic;
	signal t2_0 : std_logic;
	signal t1_0 : std_logic;
	signal t2_1 : std_logic;
	signal z0   : std_logic;
	signal t2_2 : std_logic;
	signal t1_2 : std_logic;
	signal t2_3 : std_logic;
	signal z1   : std_logic;
	signal t2_4 : std_logic;
	signal t1_3 : std_logic;
	signal t0_2 : std_logic;
	signal t1_4 : std_logic;
	signal z2   : std_logic;
	signal t1_5 : std_logic;
	signal t1_6 : std_logic;
	signal z3   : std_logic;
begin
	t0_0 <= input(0) and input(2);
	t0_1 <= t0_0 xor input(1);
	t2_0 <= not input(0);
	t1_0 <= input(3) xor t2_0;
	t2_1 <= t0_1 or t1_0;
	z0   <= input(0) xor t2_1;
	t2_2 <= input(2) and t0_1;
	t1_2 <= t1_0 xor t2_2;
	t2_3 <= input(3) or z0;
	z1   <= t2_3 xor t1_2;
	t2_4 <= input(2) xor t1_2;
	t1_3 <= t1_2 xor input(3);
	t0_2 <= t0_1 xor t2_4;
	t1_4 <= t1_3 or t0_2;
	z2   <= t2_4 xor t1_4;
	t1_5 <= z1 and z2;
	t1_6 <= t1_5 or z0;
	z3   <= t0_2 xor t1_6;

	output <= z3 & z2 & z1 & z0;

-- with input select
--	output <=
--		x"1" when x"0",
--		x"B" when x"1",
--		x"9" when x"2",
--		x"C" when x"3",
--		x"D" when x"4",
--		x"6" when x"5",
--		x"F" when x"6",
--		x"3" when x"7",
--		x"E" when x"8",
--		x"8" when x"9",
--		x"7" when x"A",
--		x"4" when x"B",
--		x"A" when x"C",
--		x"2" when x"D",
--		x"5" when x"E",
--		x"0" when x"F",
--		
--		"----" when others;

end architecture RTL;
