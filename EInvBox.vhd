-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity EInvBox is
	port(
		input  : in  std_logic_vector(3 downto 0);
		output : out std_logic_vector(3 downto 0)
	);
end entity EInvBox;

architecture RTL of EInvBox is
	signal t0_0 : std_logic;
	signal t1_0 : std_logic;
	signal t1_1 : std_logic;
	signal t2_0 : std_logic;
	signal z3   : std_logic;
	signal t2_1 : std_logic;
	signal t3_0 : std_logic;
	signal t3_1 : std_logic;
	signal t3_2 : std_logic;
	signal z0   : std_logic;
	signal t2_2 : std_logic;
	signal t3_3 : std_logic;
	signal t4_0 : std_logic;
	signal t1_2 : std_logic;
	signal t2_3 : std_logic;
	signal z2   : std_logic;
	signal t0_1 : std_logic;
	signal z1   : std_logic;
begin
	t0_0 <= not input(0);
	t1_0 <= input(0) or input(1);
	t1_1 <= t1_0 xor input(3);
	t2_0 <= input(2) and t1_1;
	z3   <= t0_0 xor t2_0;
	t2_1 <= input(0) and input(2);
	t3_0 <= input(0) or input(3);
	t3_1 <= t3_0 xor t2_1;
	t3_2 <= t3_1 and input(1);
	z0   <= t0_0 xor t3_2;
	t2_2 <= t2_1 xor input(1);
	t3_3 <= input(2) xor t0_0;
	t4_0 <= z3 xor t2_2;
	t1_2 <= t1_1 and t4_0;
	t2_3 <= t2_2 or t1_2;
	z2   <= t3_3 xor t1_2;
	t0_1 <= t0_0 or input(3);
	z1   <= t0_1 xor t2_3;

	output <= z3 & z2 & z1 & z0;

--with input select
--	output <=
--		x"F" when x"0",
--		x"0" when x"1",
--		x"D" when x"2",
--		x"7" when x"3",
--		x"B" when x"4",
--		x"E" when x"5",
--		x"5" when x"6",
--		x"A" when x"7",
--		x"9" when x"8",
--		x"2" when x"9",
--		x"C" when x"A",
--		x"1" when x"B",
--		x"3" when x"C",
--		x"4" when x"D",
--		x"8" when x"E",
--		x"6" when x"F",
--		
--		"----" when others;

end architecture RTL;
