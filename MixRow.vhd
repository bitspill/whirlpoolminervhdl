-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MixRow is
	port(
		input  : in std_logic_vector(63 downto 0);
		output : out std_logic_vector(63 downto 0)
	);
end entity MixRow;

architecture RTL of MixRow is
	signal a0   : std_logic_vector(7 downto 0);
	signal a1   : std_logic_vector(7 downto 0);
	signal a2   : std_logic_vector(7 downto 0);
	signal a3   : std_logic_vector(7 downto 0);
	signal a4   : std_logic_vector(7 downto 0);
	signal a5   : std_logic_vector(7 downto 0);
	signal a6   : std_logic_vector(7 downto 0);
	signal a7   : std_logic_vector(7 downto 0);
	signal b0   : std_logic_vector(7 downto 0);
	signal b1   : std_logic_vector(7 downto 0);
	signal b2   : std_logic_vector(7 downto 0);
	signal b3   : std_logic_vector(7 downto 0);
	signal b4   : std_logic_vector(7 downto 0);
	signal b5   : std_logic_vector(7 downto 0);
	signal b6   : std_logic_vector(7 downto 0);
	signal b7   : std_logic_vector(7 downto 0);
	signal a1_9 : std_logic_vector(7 downto 0);
	signal a2_2 : std_logic_vector(7 downto 0);
	signal a3_5 : std_logic_vector(7 downto 0);
	signal a4_8 : std_logic_vector(7 downto 0);
	signal a6_4 : std_logic_vector(7 downto 0);
	signal a2_9 : std_logic_vector(7 downto 0);
	signal a3_2 : std_logic_vector(7 downto 0);
	signal a4_5 : std_logic_vector(7 downto 0);
	signal a5_8 : std_logic_vector(7 downto 0);
	signal a7_4 : std_logic_vector(7 downto 0);
	signal a3_9 : std_logic_vector(7 downto 0);
	signal a4_2 : std_logic_vector(7 downto 0);
	signal a5_5 : std_logic_vector(7 downto 0);
	signal a6_8 : std_logic_vector(7 downto 0);
	signal a0_4 : std_logic_vector(7 downto 0);
	signal a4_9 : std_logic_vector(7 downto 0);
	signal a5_2 : std_logic_vector(7 downto 0);
	signal a6_5 : std_logic_vector(7 downto 0);
	signal a7_8 : std_logic_vector(7 downto 0);
	signal a1_4 : std_logic_vector(7 downto 0);
	signal a5_9 : std_logic_vector(7 downto 0);
	signal a6_2 : std_logic_vector(7 downto 0);
	signal a7_5 : std_logic_vector(7 downto 0);
	signal a0_8 : std_logic_vector(7 downto 0);
	signal a2_4 : std_logic_vector(7 downto 0);
	signal a6_9 : std_logic_vector(7 downto 0);
	signal a7_2 : std_logic_vector(7 downto 0);
	signal a0_5 : std_logic_vector(7 downto 0);
	signal a1_8 : std_logic_vector(7 downto 0);
	signal a3_4 : std_logic_vector(7 downto 0);
	signal a7_9 : std_logic_vector(7 downto 0);
	signal a0_2 : std_logic_vector(7 downto 0);
	signal a1_5 : std_logic_vector(7 downto 0);
	signal a2_8 : std_logic_vector(7 downto 0);
	signal a4_4 : std_logic_vector(7 downto 0);
	signal a0_9 : std_logic_vector(7 downto 0);
	signal a1_2 : std_logic_vector(7 downto 0);
	signal a2_5 : std_logic_vector(7 downto 0);
	signal a3_8 : std_logic_vector(7 downto 0);
	signal a5_4 : std_logic_vector(7 downto 0);

begin
	a0 <= input(63 downto 56);
	a1 <= input(55 downto 48);
	a2 <= input(47 downto 40);
	a3 <= input(39 downto 32);
	a4 <= input(31 downto 24);
	a5 <= input(23 downto 16);
	a6 <= input(15 downto 8);
	a7 <= input(7 downto 0);

	mult1_9 : entity work.MultiplyByte9 port map(input => a1, output => a1_9); -- cell 0
	mult2_2 : entity work.MultiplyByte2 port map(input => a2, output => a2_2);
	mult3_5 : entity work.MultiplyByte5 port map(input => a3, output => a3_5);
	mult4_8 : entity work.MultiplyByte8 port map(input => a4, output => a4_8);
	mult6_4 : entity work.MultiplyByte4 port map(input => a6, output => a6_4);
	mult2_9 : entity work.MultiplyByte9 port map(input => a2, output => a2_9); -- cell 1
	mult3_2 : entity work.MultiplyByte2 port map(input => a3, output => a3_2);
	mult4_5 : entity work.MultiplyByte5 port map(input => a4, output => a4_5);
	mult5_8 : entity work.MultiplyByte8 port map(input => a5, output => a5_8);
	mult7_4 : entity work.MultiplyByte4 port map(input => a7, output => a7_4);
	mult3_9 : entity work.MultiplyByte9 port map(input => a3, output => a3_9); -- cell 2
	mult4_2 : entity work.MultiplyByte2 port map(input => a4, output => a4_2);
	mult5_5 : entity work.MultiplyByte5 port map(input => a5, output => a5_5);
	mult6_8 : entity work.MultiplyByte8 port map(input => a6, output => a6_8);
	mult0_4 : entity work.MultiplyByte4 port map(input => a0, output => a0_4);
	mult4_9 : entity work.MultiplyByte9 port map(input => a4, output => a4_9); -- cell 3
	mult5_2 : entity work.MultiplyByte2 port map(input => a5, output => a5_2);
	mult6_5 : entity work.MultiplyByte5 port map(input => a6, output => a6_5);
	mult7_8 : entity work.MultiplyByte8 port map(input => a7, output => a7_8);
	mult1_4 : entity work.MultiplyByte4 port map(input => a1, output => a1_4);
	mult5_9 : entity work.MultiplyByte9 port map(input => a5, output => a5_9); -- cell 4
	mult6_2 : entity work.MultiplyByte2 port map(input => a6, output => a6_2);
	mult7_5 : entity work.MultiplyByte5 port map(input => a7, output => a7_5);
	mult0_8 : entity work.MultiplyByte8 port map(input => a0, output => a0_8);
	mult2_4 : entity work.MultiplyByte4 port map(input => a2, output => a2_4);
	mult6_9 : entity work.MultiplyByte9 port map(input => a6, output => a6_9); -- cell 5
	mult7_2 : entity work.MultiplyByte2 port map(input => a7, output => a7_2);
	mult0_5 : entity work.MultiplyByte5 port map(input => a0, output => a0_5);
	mult1_8 : entity work.MultiplyByte8 port map(input => a1, output => a1_8);
	mult3_4 : entity work.MultiplyByte4 port map(input => a3, output => a3_4);
	mult7_9 : entity work.MultiplyByte9 port map(input => a7, output => a7_9); -- cell 6
	mult0_2 : entity work.MultiplyByte2 port map(input => a0, output => a0_2);
	mult1_5 : entity work.MultiplyByte5 port map(input => a1, output => a1_5);
	mult2_8 : entity work.MultiplyByte8 port map(input => a2, output => a2_8);
	mult4_4 : entity work.MultiplyByte4 port map(input => a4, output => a4_4);
	mult0_9 : entity work.MultiplyByte9 port map(input => a0, output => a0_9); -- cell 7
	mult1_2 : entity work.MultiplyByte2 port map(input => a1, output => a1_2);
	mult2_5 : entity work.MultiplyByte5 port map(input => a2, output => a2_5);
	mult3_8 : entity work.MultiplyByte8 port map(input => a3, output => a3_8);
	mult5_4 : entity work.MultiplyByte4 port map(input => a5, output => a5_4);

	b0 <= a0 xor a1_9 xor a2_2 xor a3_5 xor a4_8 xor a5 xor a6_4 xor a7; -- b0 = a0*1 + a1*9 + a2*2 + a3*5 + a4*8 + a5*1 + a6*4 + a7*1
	b1 <= a0 xor a1 xor a2_9 xor a3_2 xor a4_5 xor a5_8 xor a6 xor a7_4; -- b1 = a0*1 + a1*1 + a2*9 + a3*2 + a4*5 + a5*8 + a6*1 + a7*4
	b2 <= a0_4 xor a1 xor a2 xor a3_9 xor a4_2 xor a5_5 xor a6_8 xor a7; -- b2 = a0*4 + a1*1 + a2*1 + a3*9 + a4*2 + a5*5 + a6*8 + a7*1
	b3 <= a0 xor a1_4 xor a2 xor a3 xor a4_9 xor a5_2 xor a6_5 xor a7_8; -- b3 = a0*1 + a1*4 + a2*1 + a3*1 + a4*9 + a5*2 + a6*5 + a7*8
	b4 <= a0_8 xor a1 xor a2_4 xor a3 xor a4 xor a5_9 xor a6_2 xor a7_5; -- b4 = a0*8 + a1*1 + a2*4 + a3*1 + a4*1 + a5*9 + a6*2 + a7*5
	b5 <= a0_5 xor a1_8 xor a2 xor a3_4 xor a4 xor a5 xor a6_9 xor a7_2; -- b5 = a0*5 + a1*8 + a2*1 + a3*4 + a4*1 + a5*1 + a6*9 + a7*2
	b6 <= a0_2 xor a1_5 xor a2_8 xor a3 xor a4_4 xor a5 xor a6 xor a7_9; -- b6 = a0*2 + a1*5 + a2*8 + a3*1 + a4*4 + a5*1 + a6*1 + a7*9
	b7 <= a0_9 xor a1_2 xor a2_5 xor a3_8 xor a4 xor a5_4 xor a6 xor a7; -- b7 = a0*9 + a1*2 + a2*5 + a3*8 + a4*1 + a5*4 + a6*1 + a7*1

	output(63 downto 56) <= b0;
	output(55 downto 48) <= b1;
	output(47 downto 40) <= b2;
	output(39 downto 32) <= b3;
	output(31 downto 24) <= b4;
	output(23 downto 16) <= b5;
	output(15 downto 8)  <= b6;
	output(7 downto 0)   <= b7;

end architecture RTL;
