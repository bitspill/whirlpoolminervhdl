-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MixRows is
	port(
		input  : in  std_logic_vector(511 downto 0);
		output : out std_logic_vector(511 downto 0)
	);
end entity MixRows;

architecture RTL of MixRows is
	signal row0     : std_logic_vector(63 downto 0);
	signal row1     : std_logic_vector(63 downto 0);
	signal row2     : std_logic_vector(63 downto 0);
	signal row3     : std_logic_vector(63 downto 0);
	signal row4     : std_logic_vector(63 downto 0);
	signal row5     : std_logic_vector(63 downto 0);
	signal row6     : std_logic_vector(63 downto 0);
	signal row7     : std_logic_vector(63 downto 0);
	signal row0_out : std_logic_vector(63 downto 0);
	signal row1_out : std_logic_vector(63 downto 0);
	signal row2_out : std_logic_vector(63 downto 0);
	signal row3_out : std_logic_vector(63 downto 0);
	signal row4_out : std_logic_vector(63 downto 0);
	signal row5_out : std_logic_vector(63 downto 0);
	signal row6_out : std_logic_vector(63 downto 0);
	signal row7_out : std_logic_vector(63 downto 0);

begin
	row0 <= input(511 downto 448);
	row1 <= input(447 downto 384);
	row2 <= input(383 downto 320);
	row3 <= input(319 downto 256);
	row4 <= input(255 downto 192);
	row5 <= input(191 downto 128);
	row6 <= input(127 downto 64);
	row7 <= input(63 downto 0);

	mix0 : entity work.MixRow port map(input => row0, output => row0_out);
	mix1 : entity work.MixRow port map(input => row1, output => row1_out);
	mix2 : entity work.MixRow port map(input => row2, output => row2_out);
	mix3 : entity work.MixRow port map(input => row3, output => row3_out);
	mix4 : entity work.MixRow port map(input => row4, output => row4_out);
	mix5 : entity work.MixRow port map(input => row5, output => row5_out);
	mix6 : entity work.MixRow port map(input => row6, output => row6_out);
	mix7 : entity work.MixRow port map(input => row7, output => row7_out);

	output(511 downto 448) <= row0_out;
	output(447 downto 384) <= row1_out;
	output(383 downto 320) <= row2_out;
	output(319 downto 256) <= row3_out;
	output(255 downto 192) <= row4_out;
	output(191 downto 128) <= row5_out;
	output(127 downto 64)  <= row6_out;
	output(63 downto 0)    <= row7_out;

end architecture RTL;
