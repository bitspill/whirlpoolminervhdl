-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MultiplyByte2 is
	port(
		input  : in  std_logic_vector(7 downto 0);
		output : out std_logic_vector(7 downto 0)
	);
end entity MultiplyByte2;

architecture RTL of MultiplyByte2 is
begin
	output <= input(6) & input(5) & input(4) & (input(3) xor input(7)) & (input(2) xor input(7)) & (input(1) xor input(7)) & input(0) & input(7);

end architecture RTL;
