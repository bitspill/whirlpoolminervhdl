-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MultiplyByte5 is
	port(
		input  : in  std_logic_vector(7 downto 0);
		output : out std_logic_vector(7 downto 0)
	);
end entity MultiplyByte5;

architecture RTL of MultiplyByte5 is
	signal input4 : std_logic_vector(7 downto 0);
begin
	mult4 : entity work.MultiplyByte4
		port map(
			input  => input,
			output => input4
		);

	output <= input xor input4;

end architecture RTL;
