-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MultiplyByte8 is
	port(
		input  : in  std_logic_vector(7 downto 0);
		output : out std_logic_vector(7 downto 0)
	);
end entity MultiplyByte8;

architecture RTL of MultiplyByte8 is
begin
	output <= (input(4)) & (input(3) xor input(7)) & (input(2) xor input(7) xor input(6)) & (input(1) xor input(7) xor input(6) xor input(5)) & (input(0) xor input(6) xor input(5)) & (input(7) xor input(5)) & (input(6)) & (input(5));

end architecture RTL;
