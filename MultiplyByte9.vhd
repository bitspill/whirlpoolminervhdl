-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MultiplyByte9 is
	port(
		input  : in  std_logic_vector(7 downto 0);
		output : out std_logic_vector(7 downto 0)
	);
end entity MultiplyByte9;

architecture RTL of MultiplyByte9 is
	signal input8 : std_logic_vector(7 downto 0);
begin
	mult8 : entity work.MultiplyByte8
		port map(
			input  => input,
			output => input8
		);

	output <= input xor input8;

end architecture RTL;
