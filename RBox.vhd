-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RBox is
	port(
		input  : in  std_logic_vector(3 downto 0);
		output : out std_logic_vector(3 downto 0)
	);
end entity RBox;

architecture RTL of RBox is
	signal t0_0 : std_logic;
	signal t1_0 : std_logic;
	signal t2_0 : std_logic;
	signal t2_1 : std_logic;
	signal t3_0 : std_logic;
	signal z2   : std_logic;
	signal t2_2 : std_logic;
	signal t2_3 : std_logic;
	signal t3_1 : std_logic;
	signal z3   : std_logic;
	signal t3_2 : std_logic;
	signal t0_1 : std_logic;
	signal t1_1 : std_logic;
	signal t1_2 : std_logic;
	signal z0   : std_logic;
	signal t3_3 : std_logic;
	signal z1   : std_logic;
begin
	t0_0 <= not input(0);
	t1_0 <= input(2) and input(3);
	t2_0 <= input(0) xor t1_0;
	t2_1 <= t2_0 or input(1);
	t3_0 <= input(3) or t0_0;
	z2   <= t2_1 xor t3_0;
	t2_2 <= not input(2);
	t2_3 <= t2_2 xor t3_0;
	t3_1 <= input(1) or t2_3;
	z3   <= t1_0 xor t3_1;
	t3_2 <= t3_1 xor t0_0;
	t0_1 <= input(0) or z3;
	t1_1 <= not input(1);
	t1_2 <= t1_1 xor input(3);
	z0   <= t0_1 xor t1_2;
	t3_3 <= t3_2 or z0;
	z1   <= t2_3 xor t3_3;

	output <= z3 & z2 & z1 & z0;

--with input select
--	output <=
--		x"7" when x"0",
--		x"C" when x"1",
--		x"B" when x"2",
--		x"D" when x"3",
--		x"E" when x"4",
--		x"4" when x"5",
--		x"9" when x"6",
--		x"F" when x"7",
--		x"6" when x"8",
--		x"3" when x"9",
--		x"8" when x"A",
--		x"A" when x"B",
--		x"2" when x"C",
--		x"5" when x"D",
--		x"1" when x"E",
--		x"0" when x"F",
--		
--		"----" when others;

end architecture RTL;
