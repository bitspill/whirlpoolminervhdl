-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ShiftColumns is
	port(
		input  : in  std_logic_vector(511 downto 0);
		output : out std_logic_vector(511 downto 0)
	);
end entity ShiftColumns;

architecture RTL of ShiftColumns is
begin
	-- mappings built using Excel Voodoo
	-- made the "matrix" in Excel then manually moved
	-- stuff around and used a formula or two
	output(511 downto 504) <= input(511 downto 504);
	output(447 downto 440) <= input(447 downto 440);
	output(383 downto 376) <= input(383 downto 376);
	output(319 downto 312) <= input(319 downto 312);
	output(255 downto 248) <= input(255 downto 248);
	output(191 downto 184) <= input(191 downto 184);
	output(127 downto 120) <= input(127 downto 120);
	output(63 downto 56)   <= input(63 downto 56);
	output(503 downto 496) <= input(55 downto 48);
	output(439 downto 432) <= input(503 downto 496);
	output(375 downto 368) <= input(439 downto 432);
	output(311 downto 304) <= input(375 downto 368);
	output(247 downto 240) <= input(311 downto 304);
	output(183 downto 176) <= input(247 downto 240);
	output(119 downto 112) <= input(183 downto 176);
	output(55 downto 48)   <= input(119 downto 112);
	output(495 downto 488) <= input(111 downto 104);
	output(431 downto 424) <= input(47 downto 40);
	output(367 downto 360) <= input(495 downto 488);
	output(303 downto 296) <= input(431 downto 424);
	output(239 downto 232) <= input(367 downto 360);
	output(175 downto 168) <= input(303 downto 296);
	output(111 downto 104) <= input(239 downto 232);
	output(47 downto 40)   <= input(175 downto 168);
	output(487 downto 480) <= input(167 downto 160);
	output(423 downto 416) <= input(103 downto 96);
	output(359 downto 352) <= input(39 downto 32);
	output(295 downto 288) <= input(487 downto 480);
	output(231 downto 224) <= input(423 downto 416);
	output(167 downto 160) <= input(359 downto 352);
	output(103 downto 96)  <= input(295 downto 288);
	output(39 downto 32)   <= input(231 downto 224);
	output(479 downto 472) <= input(223 downto 216);
	output(415 downto 408) <= input(159 downto 152);
	output(351 downto 344) <= input(95 downto 88);
	output(287 downto 280) <= input(31 downto 24);
	output(223 downto 216) <= input(479 downto 472);
	output(159 downto 152) <= input(415 downto 408);
	output(95 downto 88)   <= input(351 downto 344);
	output(31 downto 24)   <= input(287 downto 280);
	output(471 downto 464) <= input(279 downto 272);
	output(407 downto 400) <= input(215 downto 208);
	output(343 downto 336) <= input(151 downto 144);
	output(279 downto 272) <= input(87 downto 80);
	output(215 downto 208) <= input(23 downto 16);
	output(151 downto 144) <= input(471 downto 464);
	output(87 downto 80)   <= input(407 downto 400);
	output(23 downto 16)   <= input(343 downto 336);
	output(463 downto 456) <= input(335 downto 328);
	output(399 downto 392) <= input(271 downto 264);
	output(335 downto 328) <= input(207 downto 200);
	output(271 downto 264) <= input(143 downto 136);
	output(207 downto 200) <= input(79 downto 72);
	output(143 downto 136) <= input(15 downto 8);
	output(79 downto 72)   <= input(463 downto 456);
	output(15 downto 8)    <= input(399 downto 392);
	output(455 downto 448) <= input(391 downto 384);
	output(391 downto 384) <= input(327 downto 320);
	output(327 downto 320) <= input(263 downto 256);
	output(263 downto 256) <= input(199 downto 192);
	output(199 downto 192) <= input(135 downto 128);
	output(135 downto 128) <= input(71 downto 64);
	output(71 downto 64)   <= input(7 downto 0);
	output(7 downto 0)     <= input(455 downto 448);

end architecture RTL;
