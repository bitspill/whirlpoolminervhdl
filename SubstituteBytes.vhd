-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SubstituteBytes is
	port(
		input  : in  std_logic_vector(511 downto 0);
		output : out std_logic_vector(511 downto 0)
	);
end entity SubstituteBytes;

architecture RTL of SubstituteBytes is
	type array_type is array (1 to 64) of std_logic_vector(3 downto 0);
	signal a   : array_type;
	signal b   : array_type;
	signal axb : array_type;
	signal axc : array_type;
	signal bxc : array_type;
	signal c   : array_type;
begin
	SBox : for i in 1 to 64 generate
		EBox_inst1 : entity work.EBox
			port map(
				input  => input(i * 8 - 1 downto i * 8 - 4),
				output => a(i)
			);
		EInvBox_inst1 : entity work.EInvBox
			port map(
				input  => input(i * 8 - 5 downto i * 8 - 8),
				output => b(i)
			);
		axb(i) <= a(i) xor b(i);
		RBox_inst1 : entity work.RBox
			port map(
				input  => axb(i),
				output => c(i)
			);
		axc(i) <= a(i) xor c(i);
		EBox_inst2 : entity work.EBox
			port map(
				input  => axc(i),
				output => output(i * 8 - 1 downto i * 8 - 4)
			);
		bxc(i) <= b(i) xor c(i);
		EInvBox_inst2 : entity work.EInvBox
			port map(
				input  => bxc(i),
				output => output(i * 8 - 5 downto i * 8 - 8)
			);
	end generate SBox;

end architecture RTL;
