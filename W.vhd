-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity W is
	generic(
		first : std_logic := '1'
	);
	port(
		plaintext  : in  std_logic_vector(511 downto 0);
		key        : in  std_logic_vector(511 downto 0);
		ciphertext : out std_logic_vector(511 downto 0)
	);
end entity W;

architecture RTL of W is
	signal r0          : std_logic_vector(511 downto 0);
	signal r1          : std_logic_vector(511 downto 0);
	signal r2          : std_logic_vector(511 downto 0);
	signal r3          : std_logic_vector(511 downto 0);
	signal r4          : std_logic_vector(511 downto 0);
	signal r5          : std_logic_vector(511 downto 0);
	signal r6          : std_logic_vector(511 downto 0);
	signal r7          : std_logic_vector(511 downto 0);
	signal r8          : std_logic_vector(511 downto 0);
	signal r9          : std_logic_vector(511 downto 0);
	signal r10         : std_logic_vector(511 downto 0);
	constant zeros_448 : std_logic_vector(447 downto 0) := (others => '0');
	constant rc1       : std_logic_vector(511 downto 0) := x"1823c6e887b8014f" & zeros_448;
	constant rc2       : std_logic_vector(511 downto 0) := x"36a6d2f5796f9152" & zeros_448;
	constant rc3       : std_logic_vector(511 downto 0) := x"60bc9b8ea30c7b35" & zeros_448;
	constant rc4       : std_logic_vector(511 downto 0) := x"1de0d7c22e4bfe57" & zeros_448;
	constant rc5       : std_logic_vector(511 downto 0) := x"157737e59ff04ada" & zeros_448;
	constant rc6       : std_logic_vector(511 downto 0) := x"58c9290ab1a06b85" & zeros_448;
	constant rc7       : std_logic_vector(511 downto 0) := x"bd5d10f4cb3e0567" & zeros_448;
	constant rc8       : std_logic_vector(511 downto 0) := x"e427418ba77d95d8" & zeros_448;
	constant rc9       : std_logic_vector(511 downto 0) := x"fbee7c66dd17479e" & zeros_448;
	constant rc10      : std_logic_vector(511 downto 0) := x"ca2dbf07ad5a8333" & zeros_448;
	signal k1          : std_logic_vector(511 downto 0);
	signal k2          : std_logic_vector(511 downto 0);
	signal k3          : std_logic_vector(511 downto 0);
	signal k4          : std_logic_vector(511 downto 0);
	signal k5          : std_logic_vector(511 downto 0);
	signal k6          : std_logic_vector(511 downto 0);
	signal k7          : std_logic_vector(511 downto 0);
	signal k8          : std_logic_vector(511 downto 0);
	signal k9          : std_logic_vector(511 downto 0);
	signal k10         : std_logic_vector(511 downto 0);
begin
	AK0 : entity work.AddKey
		port map(
			input  => plaintext,
			key    => key,
			output => r0
		);

	GenerateKeyConstants : if first = '1' generate
		KeyConstants_rnd1 : entity work.KeyConstants
			port map(
				round => 1,
				key   => k1
			);
		KeyConstants_rnd2 : entity work.KeyConstants
			port map(
				round => 2,
				key   => k2
			);
		KeyConstants_rnd3 : entity work.KeyConstants
			port map(
				round => 3,
				key   => k3
			);
		KeyConstants_rnd4 : entity work.KeyConstants
			port map(
				round => 4,
				key   => k4
			);
		KeyConstants_rnd5 : entity work.KeyConstants
			port map(
				round => 5,
				key   => k5
			);
		KeyConstants_rnd6 : entity work.KeyConstants
			port map(
				round => 6,
				key   => k6
			);
		KeyConstants_rnd7 : entity work.KeyConstants
			port map(
				round => 7,
				key   => k7
			);
		KeyConstants_rnd8 : entity work.KeyConstants
			port map(
				round => 8,
				key   => k8
			);
		KeyConstants_rnd9 : entity work.KeyConstants
			port map(
				round => 9,
				key   => k9
			);
		KeyConstants_rnd10 : entity work.KeyConstants
			port map(
				round => 10,
				key   => k10
			);
	end generate GenerateKeyConstants;

	GenerateKeyCalculation : if first = '0' generate
		KRound1 : entity work.WRound
			port map(
				input  => key,
				key    => rc1,
				output => k1
			);
		KRound2 : entity work.WRound
			port map(
				input  => k1,
				key    => rc2,
				output => k2
			);
		KRound3 : entity work.WRound
			port map(
				input  => k2,
				key    => rc3,
				output => k3
			);
		KRound4 : entity work.WRound
			port map(
				input  => k3,
				key    => rc4,
				output => k4
			);
		KRound5 : entity work.WRound
			port map(
				input  => k4,
				key    => rc5,
				output => k5
			);
		KRound6 : entity work.WRound
			port map(
				input  => k5,
				key    => rc6,
				output => k6
			);
		KRound7 : entity work.WRound
			port map(
				input  => k6,
				key    => rc7,
				output => k7
			);
		KRound8 : entity work.WRound
			port map(
				input  => k7,
				key    => rc8,
				output => k8
			);
		KRound9 : entity work.WRound
			port map(
				input  => k8,
				key    => rc9,
				output => k9
			);
		KRound10 : entity work.WRound
			port map(
				input  => k9,
				key    => rc10,
				output => k10
			);
	end generate GenerateKeyCalculation;

	Round1 : entity work.WRound
		port map(
			input  => r0,
			key    => k1,
			output => r1
		);

	Round2 : entity work.WRound
		port map(
			input  => r1,
			key    => k2,
			output => r2
		);

	Round3 : entity work.WRound
		port map(
			input  => r2,
			key    => k3,
			output => r3
		);

	Round4 : entity work.WRound
		port map(
			input  => r3,
			key    => k4,
			output => r4
		);

	Round5 : entity work.WRound
		port map(
			input  => r4,
			key    => k5,
			output => r5
		);

	Round6 : entity work.WRound
		port map(
			input  => r5,
			key    => k6,
			output => r6
		);

	Round7 : entity work.WRound
		port map(
			input  => r6,
			key    => k7,
			output => r7
		);

	Round8 : entity work.WRound
		port map(
			input  => r7,
			key    => k8,
			output => r8
		);

	Round9 : entity work.WRound
		port map(
			input  => r8,
			key    => k9,
			output => r9
		);

	Round10 : entity work.WRound
		port map(
			input  => r9,
			key    => k10,
			output => r10
		);

	ciphertext <= r10;

end architecture RTL;
