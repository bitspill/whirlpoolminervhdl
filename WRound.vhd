-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity WRound is
	port(
		input  : in  std_logic_vector(511 downto 0);
		key    : in  std_logic_vector(511 downto 0);
		output : out std_logic_vector(511 downto 0)
	);
end entity WRound;

architecture RTL of WRound is
	signal sb  : std_logic_vector(511 downto 0);
	signal sc  : std_logic_vector(511 downto 0);
	signal mr  : std_logic_vector(511 downto 0);
	signal adk : std_logic_vector(511 downto 0);
begin
	subsitute_bytes : entity work.SubstituteBytes
		port map(
			input  => input,
			output => sb
		);

	shift_columns : entity work.ShiftColumns
		port map(
			input  => sb,
			output => sc
		);

	mix_rows : entity work.MixRows
		port map(
			input  => sc,
			output => mr
		);
		
	add_round_key : entity work.AddKey
		port map(
			input  => mr,
			key    => key,
			output => adk
		);
		
	output <= adk;
end architecture RTL;
