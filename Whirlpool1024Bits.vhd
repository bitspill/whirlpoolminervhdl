-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Whirlpool1024Bits is
	port(
		input  : in  std_logic_vector(1023 downto 0);
		output : out std_logic_vector(511 downto 0)
	);
end entity Whirlpool1024Bits;

architecture RTL of Whirlpool1024Bits is
	signal m1   : std_logic_vector(511 downto 0);
	signal m2   : std_logic_vector(511 downto 0);
	constant h0 : std_logic_vector(511 downto 0) := (others => '0');
	signal h1   : std_logic_vector(511 downto 0);
	signal h1x  : std_logic_vector(511 downto 0);
	signal h2   : std_logic_vector(511 downto 0);
	signal h2x  : std_logic_vector(511 downto 0);
begin
	m1 <= input(1023 downto 512);
	m2 <= input(511 downto 0);

	W1 : entity work.W
		generic map(
			first => '1'
		)
		port map(
			plaintext  => m1,
			key        => h0,
			ciphertext => h1
		);

	h1x <= m1 xor h1 xor h0;

	W2 : entity work.W
		generic map(
			first => '0'
		)
		port map(
			plaintext  => m2,
			key        => h1x,
			ciphertext => h2
		);

	h2x <= m2 xor h2 xor h1x;

	output <= h2x;

end architecture RTL;
