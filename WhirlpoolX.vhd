-- https://bitbucket.org/bitspill/whirlpoolminervhdl

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity WhirlpoolX is
	port(
		blk_version     : in  std_logic_vector(4 * 8 - 1 downto 0); -- 4 bytes
		blk_prev_block  : in  std_logic_vector(32 * 8 - 1 downto 0); -- 32 bytes
		blk_merkle_root : in  std_logic_vector(32 * 8 - 1 downto 0); -- 32 bytes
		blk_timestamp   : in  std_logic_vector(4 * 8 - 1 downto 0); -- 4 bytes
		blk_bits        : in  std_logic_vector(4 * 8 - 1 downto 0); -- 4 bytes
		blk_nonce       : in  std_logic_vector(4 * 8 - 1 downto 0); -- 4 bytes
		hash            : out std_logic_vector(256 - 1 downto 0) -- 32 bytes
	);
end entity WhirlpoolX;

architecture RTL of WhirlpoolX is
	signal composite_input     : std_logic_vector(1023 downto 0);
	signal pre_output          : std_logic_vector(511 downto 0);
	signal rev_blk_version     : std_logic_vector(4 * 8 - 1 downto 0); -- 4 bytes
	signal rev_blk_prev_block  : std_logic_vector(32 * 8 - 1 downto 0); -- 32 bytes
	signal rev_blk_merkle_root : std_logic_vector(32 * 8 - 1 downto 0); -- 32 bytes
	signal rev_blk_timestamp   : std_logic_vector(4 * 8 - 1 downto 0); -- 4 bytes
	signal rev_blk_bits        : std_logic_vector(4 * 8 - 1 downto 0); -- 4 bytes
	signal rev_blk_nonce       : std_logic_vector(4 * 8 - 1 downto 0); -- 4 bytes

	constant zeros_127bits : std_logic_vector(126 downto 0) := (others => '0'); -- pad to odd multiple of 256
	constant zeros_246bits : std_logic_vector(245 downto 0) := (others => '0'); -- pad length sector
	constant binary_640    : std_logic_vector(9 downto 0)   := "1010000000"; -- length in bits of data
begin
	version_rev : entity work.ByteReverse4
		port map(
			input  => blk_version,
			output => rev_blk_version
		);

	timestamp_rev : entity work.ByteReverse4
		port map(
			input  => blk_timestamp,
			output => rev_blk_timestamp
		);

	bits_rev : entity work.ByteReverse4
		port map(
			input  => blk_bits,
			output => rev_blk_bits
		);

	nonce_rev : entity work.ByteReverse4
		port map(
			input  => blk_nonce,
			output => rev_blk_nonce
		);

	composite_input <= rev_blk_version & blk_prev_block & blk_merkle_root & rev_blk_timestamp & rev_blk_bits & rev_blk_nonce & '1' & zeros_127bits & zeros_246bits & binary_640;

	wp : entity work.Whirlpool1024Bits
		port map(
			input  => composite_input,
			output => pre_output
		);

	-- The resulting hash consists of splitting 384 bits of whirlpool
	-- digest and XORing them together in a way to form 256 bits of output.
	-- Behemoth block follows
	hash(255 downto 248) <= pre_output(511 downto 504) xor pre_output(383 downto 376);
	hash(247 downto 240) <= pre_output(503 downto 496) xor pre_output(375 downto 368);
	hash(239 downto 232) <= pre_output(495 downto 488) xor pre_output(367 downto 360);
	hash(231 downto 224) <= pre_output(487 downto 480) xor pre_output(359 downto 352);
	hash(223 downto 216) <= pre_output(479 downto 472) xor pre_output(351 downto 344);
	hash(215 downto 208) <= pre_output(471 downto 464) xor pre_output(343 downto 336);
	hash(207 downto 200) <= pre_output(463 downto 456) xor pre_output(335 downto 328);
	hash(199 downto 192) <= pre_output(455 downto 448) xor pre_output(327 downto 320);
	hash(191 downto 184) <= pre_output(447 downto 440) xor pre_output(319 downto 312);
	hash(183 downto 176) <= pre_output(439 downto 432) xor pre_output(311 downto 304);
	hash(175 downto 168) <= pre_output(431 downto 424) xor pre_output(303 downto 296);
	hash(167 downto 160) <= pre_output(423 downto 416) xor pre_output(295 downto 288);
	hash(159 downto 152) <= pre_output(415 downto 408) xor pre_output(287 downto 280);
	hash(151 downto 144) <= pre_output(407 downto 400) xor pre_output(279 downto 272);
	hash(143 downto 136) <= pre_output(399 downto 392) xor pre_output(271 downto 264);
	hash(135 downto 128) <= pre_output(391 downto 384) xor pre_output(263 downto 256);
	hash(127 downto 120) <= pre_output(383 downto 376) xor pre_output(255 downto 248);
	hash(119 downto 112) <= pre_output(375 downto 368) xor pre_output(247 downto 240);
	hash(111 downto 104) <= pre_output(367 downto 360) xor pre_output(239 downto 232);
	hash(103 downto 96)  <= pre_output(359 downto 352) xor pre_output(231 downto 224);
	hash(95 downto 88)   <= pre_output(351 downto 344) xor pre_output(223 downto 216);
	hash(87 downto 80)   <= pre_output(343 downto 336) xor pre_output(215 downto 208);
	hash(79 downto 72)   <= pre_output(335 downto 328) xor pre_output(207 downto 200);
	hash(71 downto 64)   <= pre_output(327 downto 320) xor pre_output(199 downto 192);
	hash(63 downto 56)   <= pre_output(319 downto 312) xor pre_output(191 downto 184);
	hash(55 downto 48)   <= pre_output(311 downto 304) xor pre_output(183 downto 176);
	hash(47 downto 40)   <= pre_output(303 downto 296) xor pre_output(175 downto 168);
	hash(39 downto 32)   <= pre_output(295 downto 288) xor pre_output(167 downto 160);
	hash(31 downto 24)   <= pre_output(287 downto 280) xor pre_output(159 downto 152);
	hash(23 downto 16)   <= pre_output(279 downto 272) xor pre_output(151 downto 144);
	hash(15 downto 8)    <= pre_output(271 downto 264) xor pre_output(143 downto 136);
	hash(7 downto 0)     <= pre_output(263 downto 256) xor pre_output(135 downto 128);
end architecture RTL;
